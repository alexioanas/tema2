import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {AddProduct} from './components/AddProduct';
import {ProductList} from './components/ProductList';


class App extends Component {
  constructor(props){
    super(props);
    this.state={};
    this.state.products=[];
  }
  
  onItemAdded = (product)=>{
    let newProducts=this.state.products;
    newProducts.push(product);
    this.setState({
      products:newProducts
    })
  }
  
  componentWillMount(){
    const url = 'https://examen1-alexioanas.c9users.io/get-all'
    fetch(url).then((res) => {
      return res.json();
    }).then((products) =>{
      this.setState({
        products: products
      })
    })
  }
  
  render() {
    return (
      <React.Fragment>
        <AddProduct itemAdd={this.onItemAdded}/>
        <ProductList title="Product" source={this.state.products}/>
      </React.Fragment>
    );
  }
}

export default App;
